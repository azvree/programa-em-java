import java.util.ArrayList;
import java.util.Scanner;

public class criaMatriz {

    public static void main(String[] args) {
        Scanner texto = new Scanner(System.in);
        String calc;
        ArrayList<String> preMatriz = new ArrayList<>();
        // criar uma lista com caracteres digitados, e caso FIM, encerra e passa pro proximo passo
        System.out.println("Entre com os caracteres para criação da matriz, para encerrar digite FIM: ");
        calc = texto.nextLine();
        while (!calc.equals("FIM") && !calc.equals("fim")) {
            preMatriz.add(calc);
            calc = texto.nextLine();

        }
        // cria uma matriz com o valor arredondado pra cima do tamanho da lista
        int matrizTamanho = (int) Math.ceil(Math.sqrt(preMatriz.size()));
        Character[][] matriz = new Character[matrizTamanho][matrizTamanho];
        //insere o conteudo da lista na matriz e caso seja invalido coloca um item ""(vazio)
        int x, y, i = 0;
        for (x = 0; x < matrizTamanho; x++) {
            for (y = 0; y < matrizTamanho; y++) {
                if (i >= preMatriz.size()) {
                    matriz[x][y] = '\0';
                } else {
                    matriz[x][y] = preMatriz.get(i).charAt(0);
                }
                i++;

            }

        }

        //pegar a string e verificar na matriz
        Scanner scPalavra = new Scanner(System.in);
        System.out.println("Entre com a palavra que deseja procurar: ");
        String palavra = scPalavra.nextLine();
        //comparar a string com a matriz, acha o primeiro resultado e depois continua pra achar os demais
        for (x = 0; x < matrizTamanho; x++) {
            for (y = 0; y < matrizTamanho; y++) {
                if (matriz[x][y] == palavra.charAt(0)) {
                    if(verifica(matriz, x, y, 0, palavra)){
                        System.out.println("True");

                    }
                    else{ System.out.println("False");}
                }
            }
        }

    }
    //verifica se o indice está na borda, se passar da borda ele restringe
    static boolean verificaBordas(Character[][] matriz, int x, int y) {
        return x < 0 || y < 0 || x >= matriz.length || y >= matriz.length;
    }

    //verifica os vizinhos, porem tem algum problema que nao consigo visualizar que só funciona com 2 caracteres de lado y+1 e y-1
    static boolean verifica(Character[][] matriz, int x, int y, int index, String palavra){
    // verifica se ponto(x,y) é valido , caso nao seja ele retorna falso
        if(verificaBordas(matriz,x,y))
            return false;
    //verifica se conteudo não é o procurado, se não for retorna a busca
        if (matriz[x][y] != palavra.charAt(index)){
            return false;
        }

    // caso o indice seja igual ao tamanho da palavra -1 e igual o caracter quer dizer que chegou ao fim
        if ((index == palavra.length()-1) && (matriz[x][y] == palavra.charAt(index))) {
            return true;
        }
        // aumenta o indice pra fazer a proxima procura
        index=+1;

        // apaga o conteudo para nao ser procurado novamente
        matriz[x][y] = '\0';

        //faz a procura em todos os 8 pontos(x,y) no entorno do caracter achado, se não achar retorna verdadeiro
        if (verifica(matriz, x, y - 1, index, palavra) ||
                verifica(matriz, x, y + 1, index, palavra) ||
                verifica(matriz, x + 1, y + 1, index, palavra) ||
                verifica(matriz, x - 1, y, index, palavra) ||
                verifica(matriz, x + 1, y, index, palavra) ||
                verifica(matriz, x - 1, y - 1, index, palavra) ||
                verifica(matriz, x - 1, y + 1, index, palavra)||
                verifica(matriz, x + 1, y - 1, index, palavra)){
            return true ;
        }

    //refaz o programa
        return verifica(matriz, x, y, index, palavra);
    }
}
